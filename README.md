# STPA Tools

This repository contains some python tools which implement useful automations around the STPA YAML format.

The documentation for the [STPA YAML format can be found here](doc/FORMAT.md), and a basic
guide to [drafting your first STPA analysis can be found here](doc/process.md).

## Installation

To install, run

```bash
python setup.py install
```

This will install 2 scripts:

* stpa-validate
* stpa-render

## Usage

### stpa-validate

stpa-validate will validate that the STPA document(s) can be loaded, ensure the yml formatting is correct, check that all identifiers are unique, and observe completeness. To use the tool, just pass all of the files you wish to check as arguments:

```bash
stpa-validate <input-files>
```

Note: the tool will only output errors, so there will be no output if the validation has passed

### stpa-render

stpa-render will render all of the STPA yml files into a single markdown file which is easier to read.

```bash
stpa-render -o <output-file-name> <input-files>
```