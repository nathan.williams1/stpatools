#!/usr/bin/env python3

import os
import sys


###################################
# Ensure we have a version number #
###################################

# Add local directory to the path, in order to be able to import versioneer
sys.path.append(os.path.dirname(__file__))
import versioneer  # pylint: disable=wrong-import-position

version = versioneer.get_version()

if version.startswith("0+untagged"):
    print(
        "Your git repository has no tags - cannot determine version. Please run `git fetch --tags`.", file=sys.stderr,
    )
    sys.exit(1)


##################################################################
# Python requirements
##################################################################
REQUIRED_PYTHON_MAJOR = 3
REQUIRED_PYTHON_MINOR = 7

if sys.version_info[0] != REQUIRED_PYTHON_MAJOR or sys.version_info[1] < REQUIRED_PYTHON_MINOR:
    print(f"Python >= {REQUIRED_PYTHON_MAJOR}.{REQUIRED_PYTHON_MINOR} is required")
    sys.exit(1)

try:
    from setuptools import setup, find_packages
    from setuptools.command.easy_install import ScriptWriter
except ImportError:
    print(
        "setuptools is required in order to build. Install it using"
        " your package manager (usually python3-setuptools) or via pip (pip3"
        " install setuptools)."
    )
    sys.exit(1)


#####################################################
#                   Entry Points                    #
#####################################################
#
# Because setuptools... there is no way to pass an option to
# the setup.py explicitly at install time.
#
# So screw it, lets just use an env var.
install_entry_points = {
    "console_scripts": ["stpa-validate = stpatools._validate:validate", "stpa-render = stpatools._render:render",],
}

#####################################################
#    Monkey-patching setuptools for performance     #
#####################################################
#
# The template of easy_install.ScriptWriter is inefficient in our case as it
# imports pkg_resources. Patching the template only doesn't work because of the
# old string formatting used (%). This forces us to overwrite the class function
# as well.
#
# The patch was inspired from https://github.com/ninjaaron/fast-entry_points
# which we believe was also inspired from the code from `setuptools` project.
TEMPLATE = """\
# -*- coding: utf-8 -*-
import sys

from {0} import {1}

if __name__ == '__main__':
    sys.exit({2}())"""


# Modify the get_args() function of the ScriptWriter class
# Note: the pylint no-member warning has been disabled as the functions: get_header(),
# ensure_safe_name() and _get_script_args() are all members of this class.
# pylint: disable=no-member,protected-access
@classmethod
def get_args(cls, dist, header=None):
    if header is None:
        header = cls.get_header()
    for name, ep in dist.get_entry_map("console_scripts").items():
        cls._ensure_safe_name(name)
        script_text = TEMPLATE.format(ep.module_name, ep.attrs[0], ".".join(ep.attrs))
        args = cls._get_script_args("console", name, header, script_text)
        for res in args:
            yield res


ScriptWriter.get_args = get_args


#####################################################
#               Gather requirements                 #
#####################################################
with open("requirements/requirements.txt", encoding="utf-8") as install_reqs:
    install_requires = install_reqs.read().splitlines()


#####################################################
#     Prepare package description from README       #
#####################################################
with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "README.md"), encoding="utf-8") as readme:
    long_description = readme.read()


#####################################################
#             Main setup() Invocation               #
#####################################################
setup(
    name="stpatools",
    version=version,
    cmdclass=versioneer.get_cmdclass(),
    author="STPA Developers",
    classifiers=[
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Topic :: Software Development :: Build Tools",
    ],
    description="Tools for processing STPA YAML documents",
    license="N/A",
    long_description=long_description,
    long_description_content_type="text/x-rst; charset=UTF-8",
    url="file:///",
    python_requires=f"~={REQUIRED_PYTHON_MAJOR}.{REQUIRED_PYTHON_MINOR}",
    package_dir={"": "src"},
    packages=find_packages(where="src", exclude=("tests", "tests.*")),
    install_requires=install_requires,
    entry_points=install_entry_points,
    zip_safe=False,
)
