import os
import pytest
from click.testing import CliRunner
from stpatools._validate import validate

DATA_DIR = os.path.dirname(os.path.realpath(__file__))


@pytest.mark.datafiles(DATA_DIR)
def test_missing_file(datafiles):
    runner = CliRunner()
    result = runner.invoke(validate, os.path.join(str(datafiles), "no-file-at-all.yml"))
    assert result.exit_code != 0
