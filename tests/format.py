import os
import pytest
from click.testing import CliRunner
from stpatools._validate import validate

DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "format")


@pytest.mark.datafiles(DATA_DIR)
def test_invalid_yaml(datafiles):
    runner = CliRunner()
    result = runner.invoke(validate, [os.path.join(str(datafiles), "invalid-yaml.yml")])
    print(result.output)
    assert result.exit_code != 0
    assert "Malformed YAML" in result.output


@pytest.mark.datafiles(DATA_DIR)
@pytest.mark.parametrize(
    "filename,provenance",
    [
        ("invalid-key-toplevel.yml", "line 2 column 6"),
        ("invalid-key-loss.yml", "line 3 column 11"),
        ("invalid-key-hazard.yml", "line 9 column 11"),
        ("invalid-key-constraint.yml", "line 15 column 8"),
        ("invalid-key-responsibility.yml", "line 25 column 13"),
        ("invalid-key-component.yml", "line 6 column 13"),
        ("invalid-key-control-action.yml", "line 14 column 16"),
        ("invalid-key-unsafe-control-action.yml", "line 28 column 21"),
        ("invalid-key-controller-constraint.yml", "line 33 column 14"),
        ("invalid-key-loss-scenario.yml", "line 35 column 18"),
    ],
    ids=[
        "toplevel",
        "loss",
        "hazard",
        "constraint",
        "responsibility",
        "component",
        "control-action",
        "unsafe-control-action",
        "controller-constraint",
        "loss-scenario",
    ],
)
def test_invalid_key(datafiles, filename, provenance):
    runner = CliRunner()
    result = runner.invoke(validate, [os.path.join(str(datafiles), filename)])
    print(result.output)
    assert result.exit_code != 0
    assert provenance in result.output
    assert "Invalid key" in result.output


@pytest.mark.datafiles(DATA_DIR)
def test_invalid_value_toplevel(datafiles):
    runner = CliRunner()
    result = runner.invoke(validate, [os.path.join(str(datafiles), "invalid-value-toplevel.yml")])
    print(result.output)
    assert result.exit_code != 0

    # The toplevel does not have any provenance
    assert "YAML file has content of type" in result.output


@pytest.mark.datafiles(DATA_DIR)
@pytest.mark.parametrize(
    "filename,provenance",
    [
        ("invalid-value-loss-text.yml", "line 2 column 2"),
        ("invalid-value-hazard-identifier.yml", "line 6 column 2"),
        ("invalid-value-constraint-hazards.yml", "line 13 column 11"),
        ("invalid-value-responsibility-constraints.yml", "line 25 column 4"),
        ("invalid-value-component-actions.yml", "line 7 column 4"),
        ("invalid-value-control-action-target.yml", "line 8 column 4"),
        ("invalid-value-unsafe-control-action-hazards.yml", "line 26 column 15"),
        ("invalid-value-controller-constraint-ucas.yml", "line 33 column 6"),
    ],
    ids=[
        "loss-text",
        "hazard-id",
        "constraint-hazards",
        "responsibility-constraints",
        "component-control-actions",
        "control-action-target",
        "unsafe-control-action-hazards",
        "controller-constraint-ucas",
    ],
)
def test_invalid_value(datafiles, filename, provenance):
    runner = CliRunner()
    result = runner.invoke(validate, [os.path.join(str(datafiles), filename)])
    print(result.output)
    assert result.exit_code != 0
    assert provenance in result.output


@pytest.mark.datafiles(DATA_DIR)
def test_multi_file_references(datafiles):
    runner = CliRunner()

    filenames = [
        os.path.join(str(datafiles), filename)
        for filename in [
            "grouped-hazards-losses.yml",
            "grouped-constraints.yml",
            "grouped-component-rider.yml",
            "grouped-component-pony.yml",
        ]
    ]

    result = runner.invoke(validate, filenames)
    print(result.output)
    assert result.exit_code == 0


@pytest.mark.datafiles(DATA_DIR)
@pytest.mark.parametrize(
    "filename,provenance",
    [
        ("missing-loss-from-hazard.yml", "line 3 column 2"),
        ("missing-hazard-from-system-constraint.yml", "line 3 column 2"),
        ("missing-constraint-from-responsibility.yml", "line 15 column 2"),
        ("missing-component-from-responsibility.yml", "line 16 column 2"),
        ("missing-component-from-control-action.yml", "line 8 column 4"),
        ("missing-hazard-from-uca.yml", "line 15 column 6"),
        ("missing-uca-from-controller-constraint.yml", "line 15 column 4"),
    ],
    ids=[
        "loss-from-hazard",
        "hazard-from-system-constraint",
        "constraint-from-responsibility",
        "component-from-responsibility",
        "component-from-control-action",
        "hazard-from-uca",
        "uca-from-controller-constraint",
    ],
)
def test_missing_record(datafiles, filename, provenance):
    runner = CliRunner()
    result = runner.invoke(validate, [os.path.join(str(datafiles), filename)])
    print(result.output)
    assert result.exit_code != 0
    assert provenance in result.output
