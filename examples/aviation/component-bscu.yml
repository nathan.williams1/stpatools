
Components:

- Identifier: BSCU
  Text: Brake System Control Unit

  Control Actions:

  - Identifier: BSCU-CA-1
    Text: Brake
    Target: Brake

    Unsafe Control Actions:

    - Identifier: BSCU-UCA-001
      Text: >
        Autobrake does not provide the Brake control action during landing roll when the BSCU is armed
      Reason: Not Providing
      Hazards: [H-4.1]
      Scenarios:
      - Identifier: BSCU-LS-001
        Text: >
          The BSCU Autobrake physical controller fails during landing roll when BSCU is armed,
          causing the Brake control action to not be provided. As a result, insufficient deceleration
          may be provided upon landing.

    - Identifier: BSCU-UCA-002
      Text: >
        Autobrake provides Brake control action during a normal takeoff
      Reason: Providing
      Hazards: [H-4.3, H-4.6]
      Scenarios:
      - Identifier: BSCU-LS-002.1
        Text: >
          The BSCU is armed and the aircraft begins landing roll. The BSCU does not provide the Brake control
          action because the BSCU incorrectly believes the aircraft has already come to a stop. This flawed process
          model will occur if the received feedback momentarily indicates zero speed during landing roll.
          The received feedback may momentarily indicate zero speed during anti-skid operation, even though the
          aircraft is not stopped.
      - Identifier: BSCU-LS-002.2
        Text: >
          The BSCU is armed and the aircraft begins landing roll. The BSCU does not provide the Brake control action
          because the BSCU incorrectly believes the aircraft is in the air and has not touched down. This flawed process
          model will occur if the touchdown indication is not received upon touchdown. The touchdown indication may
          not be received when needed if any of the following occur: Wheels hydroplane due to a wet runway (insufficient wheel speed),
          Wheel speed feedback is delayed due to filtering used, Conflicting air/ground indications due to crosswind landing,
          Failure of wheel speed sensorsoFailure of air/ground switches.

    - Identifier: BSCU-UCA-003
      Text: >
        Autobrake provides the Brake control action too late (>TBD seconds) after touchdown
      Reason: Timing
      Hazards: [H-4.1]
      Scenarios:
      - Identifier: BSCU-LS-003
        Text: >
          The aircraft lands, but processing delays within the BSCU result in the Brake control action
          being provided too late. As a result, insufficient deceleration may be provided upon landing.

    - Identifier: BSCU-UCA-004
      Text: >
        Autobrake stops providing the Brake control action too early (before TBD taxi speed attained) when aircraft lands
      Reason: Duration
      Hazards: [H-4.1]

    - Identifier: BSCU-UCA-005
      Text: >
        Autobrake provides Brake control action with an insufficient level of braking during landing roll
      Reason: Providing
      Hazards: [H-4.1]

    - Identifier: BSCU-UCA-006
      Text: >
        Autobrake provides Brake control action with directional or asymmetrical braking during landing roll
      Reason: Providing
      Hazards: [H-4.1, H-4.2]

  Controller Constraints:

  - Identifier: BSCU-CC-001
    Text: >
      Autobrake must provide the Brake control action during landing roll when the BSCU is armed
    Unsafe Control Actions: [ BSCU-UCA-001 ]

  - Identifier: BSCU-CC-002
    Text: >
      Autobrake must not provide Brake control action during a normal takeoff
    Unsafe Control Actions: [ BSCU-UCA-002 ]

  - Identifier: BSCU-CC-003
    Text: >
      Autobrake must provide the Brake control action within TBD seconds after touchdown
    Unsafe Control Actions: [ BSCU-UCA-003 ]

  - Identifier: BSCU-CC-004
    Text: >
      Autobrake must not stop providing the Brake control action before TBD taxi speed is attained during landing roll
    Unsafe Control Actions: [ BSCU-UCA-004 ]

  - Identifier: BSCU-CC-005
    Text: >
      Autobrake must not provide less than TBD level of braking during landing roll
    Unsafe Control Actions: [ BSCU-UCA-005 ]

  - Identifier: BSCU-CC-006
    Text: >
      Autobrake must not provide directional or asymmetrical braking during landing roll
    Unsafe Control Actions: [ BSCU-UCA-006 ]
