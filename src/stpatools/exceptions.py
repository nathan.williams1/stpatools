class STPAError(Exception):
    """An STPA error occurred"""


class LoadError(STPAError):
    """An error occurred while loading YAML"""


class RecordLookupError(STPAError):
    """An error looking up a record occurred"""


class RecordRegisterError(STPAError):
    """An error registering a record occurred"""
