#!/usr/bin/env python3

import sys
from typing import List
import click

from . import (
    load_stpa,
    STPAError,
    Loss,
    Hazard,
    Component,
    ControlAction,
    ControllerConstraint,
    SystemConstraint,
    Responsibility,
    Scenario,
    UnsafeControlAction,
    UnsafeControlActionType,
)
from ._print_version import print_version


@click.command(help="Render the STPA as markdown")
@click.option("--version", is_flag=True, callback=print_version, expose_value=False, is_eager=True)
@click.option(
    "--output",
    "-o",
    type=click.Path(dir_okay=False, writable=True),
    required=True,
    help="Output file to store the markdown",
)
@click.argument("files", nargs=-1, type=click.Path())
def render(files, output):
    try:
        model = load_stpa(files)
    except STPAError as e:
        click.echo(f"Error: {e}", err=True)
        sys.exit(1)

    losses: List[Loss] = []
    hazards: List[Hazard] = []
    components: List[Component] = []
    constraints: List[SystemConstraint] = []
    responsibilities: List[Responsibility] = []
    components: List[Component] = []

    # Collect and sort all of the toplevel records
    for _, record in model.records.items():
        if isinstance(record, Loss):
            losses.append(record)
        elif isinstance(record, Hazard):
            hazards.append(record)
        elif isinstance(record, SystemConstraint):
            constraints.append(record)
        elif isinstance(record, Component):
            components.append(record)
        elif isinstance(record, Responsibility):
            responsibilities.append(record)

    report_lines: List[str] = []

    # Format markdown for various records into the report_lines list
    format_losses(report_lines, losses)
    format_hazards(report_lines, hazards)
    format_system_constraints(report_lines, constraints)
    format_responsibilities(report_lines, responsibilities)
    format_components(report_lines, components)

    # Join it all into a single string and write the report
    report = "\n".join(report_lines)
    with open(output, "w", encoding="utf-8") as f:
        f.write(report)


def link_identifier(identifier: str) -> str:
    """Generate a link to an identifier"""
    link = identifier.lower()
    link = link.replace("_", "-")
    link = link.replace(" ", "-")
    return f"[{identifier}](#{link})"


def format_scenarios(report_lines: List[str], scenarios: List[Scenario]) -> None:
    """Formats the scenarios into the report lines"""
    if scenarios:
        report_lines.append("**Scenarios:**")
        for scenario in scenarios:
            text = scenario.text.replace("\n", " ")
            report_lines.append(f"* **{scenario.identifier}:** {text}")
        report_lines.append("")


def format_losses(report_lines: List[str], losses: List[Loss]) -> None:
    """Formats the losses into the report lines"""
    report_lines.append("# Losses")
    report_lines.append("")
    for loss in losses:
        report_lines.append(f"## {loss.identifier}")
        report_lines.append("")
        report_lines.append(f"{loss.text}")
        report_lines.append("")
    report_lines.append("")


def format_hazards(report_lines: List[str], hazards: List[Hazard]) -> None:
    """Formats the hazards into the report lines"""
    report_lines.append("# Hazards")
    report_lines.append("")
    for hazard in hazards:
        report_lines.append(f"## {hazard.identifier}")
        report_lines.append("")
        report_lines.append(f"{hazard.text}")
        report_lines.append("")
        if hazard.losses:
            hazard_losses = ", ".join(link_identifier(loss.identifier) for loss in hazard.losses)
            report_lines.append(f"Possible losses: {hazard_losses}")
            report_lines.append("")
        format_scenarios(report_lines, hazard.scenarios)
    report_lines.append("")


def format_system_constraints(report_lines: List[str], constraints: List[SystemConstraint]) -> None:
    """Formats the system constraints into the report lines"""
    report_lines.append("# System Constraints")
    report_lines.append("")
    report_lines.append("| Constraint | Description |")
    report_lines.append("| ---- | ---- | ---- |")
    for constraint in constraints:
        identifier = constraint.identifier.replace("\n", " ")
        description = constraint.text.replace("\n", " ")
        report_lines.append(f"| {identifier} | {description} |")
    report_lines.append("")


def format_responsibilities(report_lines: List[str], responsibilities: List[Responsibility]) -> None:
    """Formats the responsibilities into the report lines"""
    report_lines.append("# Component responsibilities")
    report_lines.append("")
    report_lines.append("| ID | Component | Constraints | Description |")
    report_lines.append("| ---- | ---- | ---- | ---- |")
    for responsibility in responsibilities:

        identifier = responsibility.identifier.replace("\n", " ")
        component = link_identifier(responsibility.component.identifier)
        constraints = ", ".join(link_identifier(constraint.identifier) for constraint in responsibility.constraints)
        description = responsibility.text.replace("\n", " ")
        report_lines.append(f"| {identifier} | {component} | {constraints} | {description} |")
    report_lines.append("")


def format_control_actions(report_lines: List[str], control_actions: List[ControlAction]) -> None:
    """Format a component's control actions into the report lines"""
    if control_actions:
        report_lines.append("## Control Actions")
        report_lines.append("")
        for action in control_actions:
            text = action.text.replace("\n", " ")
            report_lines.append(f"### {action.identifier}")
            report_lines.append("")
            report_lines.append(f"{text}")
            report_lines.append("")
            report_lines.append(f"**Target:** {link_identifier(action.target.identifier)}")
            report_lines.append("")

            if action.unsafe_control_actions:
                report_lines.append("#### Unsafe Control Actions")
                report_lines.append("")
                for unsafe in action.unsafe_control_actions:
                    text = unsafe.text.replace("\n", " ")
                    report_lines.append(f"##### {unsafe.identifier}")
                    report_lines.append("")
                    report_lines.append(f"{text}")
                    report_lines.append("")
                    report_lines.append(f"**Reason:** {unsafe.reason.value}")
                    report_lines.append("")
                    unsafe_hazards = ", ".join(link_identifier(hazard.identifier) for hazard in unsafe.hazards)
                    report_lines.append(f"**Possible Hazards:** {unsafe_hazards}")
                    report_lines.append("")
                    format_scenarios(report_lines, unsafe.scenarios)


def format_controller_constraints(report_lines: List[str], constraints: List[ControllerConstraint]) -> None:
    """Format a component's constraints into the report lines"""
    if constraints:
        report_lines.append("## Constraints")
        report_lines.append("")
        report_lines.append("| Constraint | Description | UCAs |")
        report_lines.append("| ---- | ---- | ---- |")
        for constraint in constraints:
            identifier = constraint.identifier.replace("\n", " ")
            description = constraint.text.replace("\n", " ")
            ucas = ", ".join(link_identifier(uca.identifier) for uca in constraint.unsafe_control_actions)
            report_lines.append(f"| {identifier} | {description} | {ucas} |")
        report_lines.append("")


def format_uca_summary(report_lines: List[str], component: Component) -> None:
    """Format a the summary table of all unsafe control actions report lines"""

    report_lines.append("## Unsafe Control Action Summary")
    report_lines.append("")

    #
    # NOTE: We currently omit the 'Duration' column since that is not
    #       applicable for our use case
    #
    report_lines.append("| Control Action | Providing | Not Providing | Timing | Duration |")
    report_lines.append("| ---- | ---- | ---- | ---- | ---- |")
    for action in component.control_actions:

        action_id = link_identifier(action.identifier)

        # Sort and collect the various UCA types
        providing_ucas_list: List[UnsafeControlAction] = []
        not_providing_ucas_list: List[UnsafeControlAction] = []
        timing_ucas_list: List[UnsafeControlAction] = []
        duration_ucas_list: List[UnsafeControlAction] = []
        for unsafe in action.unsafe_control_actions:
            if unsafe.reason == UnsafeControlActionType.PROVIDING:
                providing_ucas_list.append(unsafe)
            elif unsafe.reason == UnsafeControlActionType.NOT_PROVIDING:
                not_providing_ucas_list.append(unsafe)
            elif unsafe.reason == UnsafeControlActionType.TIMING:
                timing_ucas_list.append(unsafe)
            elif unsafe.reason == UnsafeControlActionType.DURATION:
                duration_ucas_list.append(unsafe)
            else:
                click.echo(
                    f"{unsafe.provenance}: Unsupported unsafe control action reason '{unsafe.reason.value}'", err=True
                )
                sys.exit(1)

        # Format a row for this control action in the table
        providing_ucas = ", ".join(link_identifier(uca.identifier) for uca in providing_ucas_list)
        not_providing_ucas = ", ".join(link_identifier(uca.identifier) for uca in not_providing_ucas_list)
        timing_ucas = ", ".join(link_identifier(uca.identifier) for uca in timing_ucas_list)
        duration_ucas = ", ".join(link_identifier(uca.identifier) for uca in duration_ucas_list)
        report_lines.append(
            f"| {action_id} | {providing_ucas} | {not_providing_ucas} | {timing_ucas} | {duration_ucas} |"
        )
    report_lines.append("")


def format_components(report_lines: List[str], components: List[Component]) -> None:
    """Formats the components into the report lines"""
    for component in components:
        report_lines.append(f"# {component.identifier}")
        report_lines.append("")
        report_lines.append(f"{component.text}")
        report_lines.append("")

        # Format the UCA summary table
        format_uca_summary(report_lines, component)

        # Format the component's control actions
        format_control_actions(report_lines, component.control_actions)
        format_controller_constraints(report_lines, component.controller_constraints)
    report_lines.append("")
