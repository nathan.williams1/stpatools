#!/usr/bin/env python3

import sys
import textwrap
import click

from . import load_stpa, STPAError
from ._print_version import print_version


@click.command(help="Validate that the STPA document can be loaded, and observe completeness")
@click.option("--version", is_flag=True, callback=print_version, expose_value=False, is_eager=True)
@click.argument("files", nargs=-1, type=click.Path())
def validate(files):

    try:
        model = load_stpa(files)
    except STPAError as e:
        click.echo(f"Error: {e}", err=True)
        sys.exit(1)

    incomplete = [str(i) for i in model.list_incomplete()]
    incomplete_report = "\n".join(incomplete)
    incomplete_report = textwrap.indent(incomplete_report, "    ")

    if incomplete:
        click.echo(f"WARNING, the data model is incomplete in the following ways:\n\n{incomplete_report}", err=True)
