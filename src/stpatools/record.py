#
# We use "annotations" which is python 3.7+, but lets us declare full
# type annotations more naturally without needing to worry about ordering
# of expressions and resulting circular dependencies.
#
from __future__ import annotations

from enum import Enum
from typing import List, Dict, TypeVar, Type
from .exceptions import RecordLookupError, RecordRegisterError
from ._yaml import Provenance


class RecordType(Enum):
    """The type of STPA record"""

    INVALID = None
    """An invalid record type"""

    LOSS = "Loss"
    """A loss record"""

    HAZARD = "Hazard"
    """A hazard record"""

    COMPONENT = "Component"
    """A component record"""

    CONTROL_ACTION = "Control Action"
    """A control action record"""

    UNSAFE_CONTROL_ACTION = "Unsafe Control Action"
    """An unsafe control action record"""

    SYSTEM_CONSTRAINT = "System Constraint"
    """A system constraint record"""

    CONTROLLER_CONSTRAINT = "Controller Constraint"
    """A controller constraint record"""

    RESPONSIBILITY = "Responsibility"
    """A responsibility record"""

    SCENARIO = "Scenario"
    """A scenario record"""


class UnsafeControlActionType(Enum):
    """The reason a control action is unsafe"""

    PROVIDING = "Providing"
    """Providing something which it might not be safe to provide"""

    NOT_PROVIDING = "Not Providing"
    """Failing to provide something which it might not be safe to not provide"""

    TIMING = "Timing"
    """Too late, early or out of order"""

    DURATION = "Duration"
    """Too long or short"""


class IncompleteReport:
    """An object to describe how a record is incomplete"""

    def __init__(self, record: Record, description: str) -> None:
        self.record: Record = record
        """The record this report pertains to"""

        self.description: str = description
        """The description explaining why this record is incomplete"""

    def __str__(self) -> str:
        return f"{self.record.provenance} ({self.record.identifier}): {self.description}"


class Record:
    """Base record type"""

    record_type: RecordType = RecordType.INVALID
    """The type of record"""

    def __init__(self, provenance: Provenance, identifier: str, text: str) -> None:
        self.provenance = provenance
        """The provenance where the record was declared"""

        self.identifier: str = identifier
        """The unique identifier of the record"""

        self.text: str = text
        """The text describing the record"""

    def list_incomplete(self) -> List[IncompleteReport]:  # pylint: disable=no-self-use
        """Abstract method to list descriptions of why the record is considered incomplete"""
        return []


class Loss(Record):
    """A Loss record"""

    record_type = RecordType.LOSS


class Scenario(Record):
    """A Scenario record"""

    record_type = RecordType.SCENARIO


class Hazard(Record):
    """A Hazard record"""

    record_type = RecordType.HAZARD

    def __init__(
        self, provenance: Provenance, identifier: str, text: str, losses: List[Loss], scenarios: List[Scenario]
    ) -> None:
        super().__init__(provenance, identifier, text)

        self.losses: List[Loss] = []
        """The losses this hazard would lead to"""

        self.scenarios: List[Scenario] = []
        """The scenarios which might lead to this hazard"""

        self.losses.extend(losses)
        self.scenarios.extend(scenarios)

    def list_incomplete(self) -> List[IncompleteReport]:
        """List descriptions of why the record is considered incomplete"""

        incomplete = []
        if not self.losses:
            incomplete.append(IncompleteReport(self, "Hazard does not incur any Loss"))
        if not self.scenarios:
            incomplete.append(IncompleteReport(self, "No scenarios explain how this hazard might occur"))

        return incomplete


class Component(Record):
    """A Component record"""

    record_type = RecordType.COMPONENT

    def __init__(self, provenance: Provenance, identifier: str, text: str) -> None:
        super().__init__(provenance, identifier, text)

        self.control_actions: List[ControlAction] = []
        """The control actions this component performs"""

        self.unsafe_control_actions: Dict[str, UnsafeControlAction] = {}
        """Lookup table for unsafe control actions on this component"""

        self.controller_constraints: List[ControllerConstraint] = []
        """The contstraints relevant to this component as a controller"""

    def list_incomplete(self) -> List[IncompleteReport]:
        """List descriptions of why the record is considered incomplete"""

        # Find any UCAs which are not covered by the component's controller constraints
        #
        unconstrained_uca_ids = [
            uca_id
            for uca_id, uca in self.unsafe_control_actions.items()
            if not any(
                constraint for constraint in self.controller_constraints if uca in constraint.unsafe_control_actions
            )
        ]

        incomplete = []
        if unconstrained_uca_ids:
            uca_id_list = ", ".join(unconstrained_uca_ids)
            incomplete.append(
                IncompleteReport(
                    self,
                    f"Component does not declare any constraints for the following unsafe control actions: {uca_id_list}",
                )
            )

        return incomplete


class UnsafeControlAction(Record):
    """An Unsafe Control Action record"""

    record_type = RecordType.UNSAFE_CONTROL_ACTION

    def __init__(
        self,
        provenance: Provenance,
        identifier: str,
        text: str,
        reason: UnsafeControlActionType,
        hazards: List[Hazard],
        scenarios: List[Scenario],
    ) -> None:
        super().__init__(provenance, identifier, text)

        self.reason: UnsafeControlActionType = reason
        """The reason for which this control action is unsafe given a described context"""

        self.hazards: List[Hazard] = []
        """The hazard(s) this control action might lead to in a worse case scenario"""

        self.scenarios: List[Scenario] = []
        """The scenarios which might lead to this unsafe control action"""

        self.hazards.extend(hazards)
        self.scenarios.extend(scenarios)

    def list_incomplete(self) -> List[IncompleteReport]:
        """List descriptions of why the record is considered incomplete"""

        incomplete = []
        if not self.hazards:
            incomplete.append(IncompleteReport(self, "Unsafe control action does not lead to any hazard"))
        if not self.scenarios:
            incomplete.append(
                IncompleteReport(self, "No scenarios explain how this unsafe control action might arise")
            )

        return incomplete


class ControlAction(Record):
    """A Control Action record"""

    record_type = RecordType.CONTROL_ACTION

    def __init__(self, provenance: Provenance, identifier: str, text: str, target: Component) -> None:
        super().__init__(provenance, identifier, text)
        self.target: Component = target
        """The target component of this control action"""

        self.unsafe_control_actions: List[UnsafeControlAction] = []
        """The list of unsafe control actions for this control action"""


class Constraint(Record):
    """A Constraint record

    This is a common abstract base class for SystemConstraint and ControllerConstraint
    """


class SystemConstraint(Constraint):
    """A System Constraint record"""

    record_type = RecordType.SYSTEM_CONSTRAINT

    def __init__(self, provenance: Provenance, identifier: str, text: str, hazards: List[Hazard],) -> None:
        super().__init__(provenance, identifier, text)
        self.hazards: List[Hazard] = []
        """The list of hazards which this system constraint prevents"""

        self.hazards.extend(hazards)

    def list_incomplete(self) -> List[IncompleteReport]:
        """List descriptions of why the record is considered incomplete"""

        incomplete = []
        if not self.hazards:
            incomplete.append(IncompleteReport(self, "System level constraint does not prevent any hazards"))

        return incomplete


class ControllerConstraint(Constraint):
    """A Controller Constraint record"""

    record_type = RecordType.CONTROLLER_CONSTRAINT

    def __init__(
        self, provenance: Provenance, identifier: str, text: str, unsafe_control_actions: List[UnsafeControlAction],
    ) -> None:
        super().__init__(provenance, identifier, text)
        self.unsafe_control_actions: List[UnsafeControlAction] = []
        """The list of unsafe control actions which this controller constraint prevents"""

        self.unsafe_control_actions.extend(unsafe_control_actions)

    def list_incomplete(self) -> List[IncompleteReport]:
        """List descriptions of why the record is considered incomplete"""

        incomplete = []
        if not self.unsafe_control_actions:
            incomplete.append(
                IncompleteReport(self, "Controller constraint does not prevent any unsafe control actions")
            )

        return incomplete


class Responsibility(Record):
    """A Responsibility record"""

    record_type = RecordType.RESPONSIBILITY

    def __init__(
        self,
        provenance: Provenance,
        identifier: str,
        text: str,
        component: Component,
        constraints: List[SystemConstraint],
    ) -> None:
        super().__init__(provenance, identifier, text)

        self.component: Component = component
        """The component this responsibility is delegated to"""

        self.constraints: List[SystemConstraint] = []
        """The system level constraints which this responsibility ensures"""

        self.constraints.extend(constraints)

    def list_incomplete(self) -> List[IncompleteReport]:
        """List descriptions of why the record is considered incomplete"""

        incomplete = []
        if not self.constraints:
            incomplete.append(IncompleteReport(self, "Responsibility does not ensure any system level constraints"))

        return incomplete


# The `_T` type annotator lets us make statements about record types
# in the abstract, we use this to instruct the static type checker that
# the lookup function returns the expect_type type.
#
_T = TypeVar("_T", bound=Record)


class Model:
    """A fully loaded data model"""

    def __init__(self) -> None:

        self.records: Dict[str, Record] = {}
        """A table of all records by unique identifier"""

    def register(self, record: Record) -> None:
        """Register a new record"""

        try:
            existing = self.records[record.identifier]
            raise RecordRegisterError(
                f"{record.provenance}: {record.record_type.value} '{record.identifier}' cannot be registered.\n"
                f"{existing.record_type.value} '{record.identifier}' was already declared at '{existing.provenance}'"
            )
        except KeyError:
            self.records[record.identifier] = record

    def lookup(self, identifier: str, expect_type: Type[_T]) -> _T:
        """Lookup a record"""

        try:
            record = self.records[identifier]
        except KeyError as e:
            raise RecordLookupError(f"No record found with the identifier '{identifier}'") from e

        if record.record_type is not expect_type.record_type:
            raise RecordLookupError(
                f"Record '{identifier}' is of type '{record.record_type.value}', expected '{expect_type.record_type.value}'"
            )

        assert isinstance(record, expect_type)

        return record

    def list_incomplete(self) -> List[IncompleteReport]:
        """List descriptions of why loaded records are considered incomplete"""

        incomplete = []
        for _, record in self.records.items():
            incomplete.extend(record.list_incomplete())

        return incomplete
