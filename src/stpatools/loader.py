from typing import List, Dict
from ruamel.yaml.comments import CommentedMap

from .exceptions import LoadError, RecordLookupError
from .record import UnsafeControlActionType
from .record import (
    Loss,
    Scenario,
    Hazard,
    SystemConstraint,
    Responsibility,
    Component,
    ControlAction,
    UnsafeControlAction,
    ControllerConstraint,
    Model,
)

from . import _yaml


###################################################################
#                        Exported loader API                      #
###################################################################
def load_stpa(files: List[str]) -> Model:
    """Loads the data model from a collection of YAML files"""

    loaded_files: Dict[str, CommentedMap] = {}
    data_model = Model()

    # Load files and validate toplevel mappings
    for filename in files:
        loaded = _yaml.load_yaml_file(filename)
        _yaml.validate_mapping(
            filename, loaded, ["Losses", "Hazards", "Constraints", "Components", "Responsibilities"]
        )
        loaded_files[filename] = loaded

    # Load the losses
    _load_losses(data_model, loaded_files)

    # Load the hazards
    _load_hazards(data_model, loaded_files)

    # Load the system constraints
    _load_system_constraints(data_model, loaded_files)

    # Shallow load the components
    _load_components_shallow(data_model, loaded_files)

    # Second pass load components, loads the control actions
    _load_component_control_actions(data_model, loaded_files)

    # Third pass loading componetns, loads the controller constraints
    _load_component_constraints(data_model, loaded_files)

    # Load the responsibilities
    _load_responsibilities(data_model, loaded_files)

    return data_model


###################################################################
#            Broken down data model loading functions             #
###################################################################
def _load_losses(data_model: Model, loaded_files: Dict[str, CommentedMap]) -> None:
    """Load the losses into the data model"""

    for filename, loaded in loaded_files.items():
        for provenance, loss_node in _yaml.sequence_items(filename, loaded, "Losses"):
            _yaml.validate_mapping(filename, loss_node, ["Identifier", "Text"])

            identifier = _yaml.load_str_key(provenance, loss_node, "Identifier")
            text = _yaml.load_str_key(provenance, loss_node, "Text")

            loss = Loss(provenance, identifier, text)
            data_model.register(loss)


def _load_scenarios(data_model: Model, filename: str, mapping: CommentedMap, key: str) -> List[Scenario]:
    """Loads and registers a list of scenarios"""

    scenarios = []
    for provenance, scenario_node in _yaml.sequence_items(filename, mapping, key):
        _yaml.validate_mapping(filename, scenario_node, ["Identifier", "Text"])
        identifier = _yaml.load_str_key(provenance, scenario_node, "Identifier")
        text = _yaml.load_str_key(provenance, scenario_node, "Text")
        scenario = Scenario(provenance, identifier, text)
        data_model.register(scenario)
        scenarios.append(scenario)

    return scenarios


def _load_hazards(data_model: Model, loaded_files: Dict[str, CommentedMap]) -> None:
    """Load the hazards into the data model"""

    for filename, loaded in loaded_files.items():
        for provenance, hazard_node in _yaml.sequence_items(filename, loaded, "Hazards"):
            _yaml.validate_mapping(filename, hazard_node, ["Identifier", "Text", "Losses", "Scenarios"])

            identifier = _yaml.load_str_key(provenance, hazard_node, "Identifier")
            text = _yaml.load_str_key(provenance, hazard_node, "Text")

            loss_ids = _yaml.load_str_list(provenance.filename, hazard_node, "Losses")
            try:
                losses = [data_model.lookup(loss_id, Loss) for loss_id in loss_ids]
            except RecordLookupError as e:
                raise LoadError(f"{provenance}: Error looking up loss: {e}") from e

            scenarios = _load_scenarios(data_model, filename, hazard_node, "Scenarios")

            hazard = Hazard(provenance, identifier, text, losses, scenarios)
            data_model.register(hazard)


def _load_system_constraints(data_model: Model, loaded_files: Dict[str, CommentedMap]) -> None:
    """Load the system constraints into the data model"""

    for filename, loaded in loaded_files.items():
        for provenance, constraint_node in _yaml.sequence_items(filename, loaded, "Constraints"):
            _yaml.validate_mapping(filename, constraint_node, ["Identifier", "Text", "Hazards"])

            identifier = _yaml.load_str_key(provenance, constraint_node, "Identifier")
            text = _yaml.load_str_key(provenance, constraint_node, "Text")

            hazard_ids = _yaml.load_str_list(provenance.filename, constraint_node, "Hazards")
            try:
                hazards = [data_model.lookup(hazard_id, Hazard) for hazard_id in hazard_ids]
            except RecordLookupError as e:
                raise LoadError(f"{provenance}: Error looking up hazard: {e}") from e

            constraint = SystemConstraint(provenance, identifier, text, hazards)
            data_model.register(constraint)


def _load_responsibilities(data_model: Model, loaded_files: Dict[str, CommentedMap]) -> None:
    """Load the responsibilities into the data model"""

    for filename, loaded in loaded_files.items():
        for provenance, responsibility_node in _yaml.sequence_items(filename, loaded, "Responsibilities"):
            _yaml.validate_mapping(filename, responsibility_node, ["Identifier", "Text", "Component", "Constraints"])

            identifier = _yaml.load_str_key(provenance, responsibility_node, "Identifier")
            text = _yaml.load_str_key(provenance, responsibility_node, "Text")

            component_id = _yaml.load_str_key(provenance, responsibility_node, "Component")
            try:
                component = data_model.lookup(component_id, Component)
            except RecordLookupError as e:
                raise LoadError(f"{provenance}: Error looking up component: {e}") from e

            constraint_ids = _yaml.load_str_list(provenance.filename, responsibility_node, "Constraints")
            try:
                constraints = [data_model.lookup(constraint_id, SystemConstraint) for constraint_id in constraint_ids]
            except RecordLookupError as e:
                raise LoadError(f"{provenance}: Error looking up system constraint: {e}") from e

            responsibility = Responsibility(provenance, identifier, text, component, constraints)
            data_model.register(responsibility)


def _load_components_shallow(data_model: Model, loaded_files: Dict[str, CommentedMap]) -> None:
    """Initial pass to load the components"""

    for filename, loaded in loaded_files.items():
        for provenance, component_node in _yaml.sequence_items(filename, loaded, "Components"):
            _yaml.validate_mapping(
                filename, component_node, ["Identifier", "Text", "Control Actions", "Controller Constraints"]
            )

            identifier = _yaml.load_str_key(provenance, component_node, "Identifier")
            text = _yaml.load_str_key(provenance, component_node, "Text")

            component = Component(provenance, identifier, text)
            data_model.register(component)


def _load_unsafe_control_actions(
    data_model: Model, component: Component, control_action: ControlAction, control_action_node: CommentedMap
) -> None:
    """Load the unsafe control actions for this control action"""

    for provenance, uca_node in _yaml.sequence_items(
        control_action.provenance.filename, control_action_node, "Unsafe Control Actions"
    ):
        _yaml.validate_mapping(provenance.filename, uca_node, ["Identifier", "Text", "Reason", "Hazards", "Scenarios"])

        identifier = _yaml.load_str_key(provenance, uca_node, "Identifier")
        text = _yaml.load_str_key(provenance, uca_node, "Text")
        reason_text = _yaml.load_str_key(provenance, uca_node, "Reason")

        if reason_text == UnsafeControlActionType.PROVIDING.value:
            reason = UnsafeControlActionType.PROVIDING
        elif reason_text == UnsafeControlActionType.NOT_PROVIDING.value:
            reason = UnsafeControlActionType.NOT_PROVIDING
        elif reason_text == UnsafeControlActionType.TIMING.value:
            reason = UnsafeControlActionType.TIMING
        elif reason_text == UnsafeControlActionType.DURATION.value:
            reason = UnsafeControlActionType.DURATION
        else:
            raise LoadError(f"{provenance}: Invalid reason '{reason_text}' specified")

        hazard_ids = _yaml.load_str_list(provenance.filename, uca_node, "Hazards")
        try:
            hazards = [data_model.lookup(hazard_id, Hazard) for hazard_id in hazard_ids]
        except RecordLookupError as e:
            raise LoadError(f"{provenance}: Error looking up hazard: {e}") from e

        scenarios = _load_scenarios(data_model, provenance.filename, uca_node, "Scenarios")

        uca = UnsafeControlAction(provenance, identifier, text, reason, hazards, scenarios)
        data_model.register(uca)

        control_action.unsafe_control_actions.append(uca)
        component.unsafe_control_actions[identifier] = uca


def _load_control_actions(data_model: Model, component: Component, component_node: CommentedMap) -> None:
    """Load the control actions for a component"""

    for provenance, control_action_node in _yaml.sequence_items(
        component.provenance.filename, component_node, "Control Actions"
    ):
        _yaml.validate_mapping(
            provenance.filename, control_action_node, ["Identifier", "Text", "Target", "Unsafe Control Actions"]
        )

        identifier = _yaml.load_str_key(provenance, control_action_node, "Identifier")
        text = _yaml.load_str_key(provenance, control_action_node, "Text")

        target_id = _yaml.load_str_key(provenance, control_action_node, "Target")
        try:
            target = data_model.lookup(target_id, Component)
            assert isinstance(target, Component)
        except RecordLookupError as e:
            raise LoadError(f"{provenance}: Error looking up target component: {e}") from e

        control_action = ControlAction(provenance, identifier, text, target)
        data_model.register(control_action)

        component.control_actions.append(control_action)

        # Load the unsafe control actions
        _load_unsafe_control_actions(data_model, component, control_action, control_action_node)


def _load_controller_constraints(data_model: Model, component: Component, component_node: CommentedMap) -> None:
    """Load the controller constraints"""

    for provenance, constraint_node in _yaml.sequence_items(
        component.provenance.filename, component_node, "Controller Constraints"
    ):
        _yaml.validate_mapping(
            provenance.filename, constraint_node, ["Identifier", "Text", "Unsafe Control Actions"],
        )

        identifier = _yaml.load_str_key(provenance, constraint_node, "Identifier")
        text = _yaml.load_str_key(provenance, constraint_node, "Text")

        uca_ids = _yaml.load_str_list(provenance.filename, constraint_node, "Unsafe Control Actions")
        try:
            ucas = [data_model.lookup(uca_id, UnsafeControlAction) for uca_id in uca_ids]
        except RecordLookupError as e:
            raise LoadError(f"{provenance}: Error looking up unsafe control action: {e}") from e

        # Validate that the controller constraint refers only to UCAs on the same component
        #
        for uca in ucas:
            if uca.identifier not in component.unsafe_control_actions:
                raise LoadError(
                    f"{provenance}: Controller Constraint refers to foreign Unsafe Control Action '{uca.identifier}' declared at: {uca.provenance}"
                )

        constraint = ControllerConstraint(provenance, identifier, text, ucas)
        data_model.register(constraint)

        component.controller_constraints.append(constraint)


def _load_component_control_actions(data_model: Model, loaded_files: Dict[str, CommentedMap]) -> None:
    """Load the control actions on components"""

    for filename, loaded in loaded_files.items():
        for provenance, component_node in _yaml.sequence_items(filename, loaded, "Components"):
            identifier = _yaml.load_str_key(provenance, component_node, "Identifier")

            # This should already be loaded from the first pass
            component = data_model.lookup(identifier, Component)
            assert isinstance(component, Component)

            # Load control actions
            _load_control_actions(data_model, component, component_node)


def _load_component_constraints(data_model: Model, loaded_files: Dict[str, CommentedMap]) -> None:
    """Fully load the constraints on components"""

    for filename, loaded in loaded_files.items():
        for provenance, component_node in _yaml.sequence_items(filename, loaded, "Components"):
            identifier = _yaml.load_str_key(provenance, component_node, "Identifier")

            # This should already be loaded from the first pass
            component = data_model.lookup(identifier, Component)
            assert isinstance(component, Component)

            # Load controller constraints
            _load_controller_constraints(data_model, component, component_node)
